# Wakumi

![Latest Stable Version: 2.0.0](https://img.shields.io/badge/version-v2.0.0-blue) ![Language: JavaScript](https://img.shields.io/badge/Language-JavaScript-yellow) ![Framework: Discord.js](https://img.shields.io/badge/Framework-Discord.js-blue)

## ⚠️ Notice ⚠️

This project is archived and all contents / features were moved to ![Aurora](https://gitlab.com/sinsa/aurora). Wakumi v2.0.0 is still functional, but won't be supported anymore. Please refer to Aurora for the same features packed together with others.

## About

Wakumi is a simple reminderbot, specifically designed for osu! tournaments which work with a scheduling sheet. It checks always at minutes :05 and :35 if there is a match in the next 30 minutes, and if there is, a reminder notification will be sent to the referees / streamers / commentators so they know they need to do something.

## Planned

If you're interested what's planned or being fixed, check out the ![Issue section](https://gitlab.com/Sinsa/wakumi/-/issues)! If you have an awesome idea what should be implemented, either tell me on Discord or just create an issue by yourself!

## Usage

The bot is currently not meant to be in public use, it is just here to give others a better understanding of how Wakumi works. If you'd like to use the bot in your tournament, just message me as described in the paragraph below and I'll be happy to help you.

## Questions?

If we have a mutual discord server, please send me a Discord direct message to my main account, _Sinsa#2674_. Otherwise write me a ingame dm on osu! to my account, _Sinsa_. Even if I'm offline, you can be sure I read all private messages I get.
