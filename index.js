const Discord = require("discord.js");
const Client = new Discord.Client();
let { tournaments } = require("./config.json");
const { token } = require("./discord.json");
const CronJob = require("cron").CronJob;
const fs = require("fs");
const readline = require("readline");
const { google } = require("googleapis");

const SCOPES = ["https://www.googleapis.com/auth/spreadsheets.readonly"];
let matches = [];

Client.once("ready", () => {
  Client.user.setPresence({
    activity: {
      name: "over you",
      type: "WATCHING",
    },
    status: "online",
  });
  console.log(`Logged in as ${Client.user.tag}`);
  console.log("<init> I am ready!");
  tournaments = tournaments.filter((tournament) => tournament.active);
  printLoadedTournaments(tournaments);
  callTournaments();
});

/**
 * CronJob gets triggered always at minute :05 and :35
 */
new CronJob("0 5,35 * * * *", callTournaments, null, true, "Europe/Berlin");

async function callTournaments() {
  for (let i = 0; i < tournaments.length; i++) {
    await main(tournaments[i]);
    matches = [];
  }
}

async function main(tournament) {
  console.log(
    new Date().toISOString(),
    `\tExecuting Reminder function for ${tournament.acronym}...`
  );
  let credentials;
  // Load client secrets from a local file.
  try {
    credentials = fs.readFileSync("credentials.json");
  } catch (e) {
    return console.log("Error loading client secret file:", err);
  }
  // Authorize a client with credentials, then call the Google Sheets API.
  authorize(JSON.parse(credentials), readSchedules);
  await sleep(3000);
  const now = Date.now() - 3600000;
  for (let i = 0; i < matches.length; i++) {
    if (matches[i].id) {
      const matchDate = getDate(matches[i].date, matches[i].time);
      if (matchDate - now > 0 && matchDate - now < 1800000) {
        let matchInMinutes;
        if (new Date(matchDate).getMinutes() < new Date(now).getMinutes()) {
          matchInMinutes =
            new Date(matchDate).getMinutes() + 60 - new Date(now).getMinutes();
        } else {
          matchInMinutes =
            new Date(matchDate).getMinutes() - new Date(now).getMinutes();
        }
        const refereechannel = await Client.channels.cache.find(
          (channel) => channel.id === tournament.channels.referee
        );
        const streamerchannel = tournament.channels.streamer
          ? await Client.channels.cache.find(
              (channel) => channel.id === tournament.channels.streamer
            )
          : null;
        const commentatorchannel = tournament.channels.commentator
          ? await Client.channels.cache.find(
              (channel) => channel.id === tournament.channels.commentator
            )
          : null;
        //Commented out for now since this is broken dead lmfao
        // if (tournament.convars.wbdPreventPingChecking) {
        //   if (
        //     !(
        //       !(matches[i].pointsplayer1 && matches[i].pointsplayer2) ||
        //       (matches[i].pointsplayer1 === 0 && matches[i].pointsplayer2 === 0)
        //     )
        //   ) {
        //     return; // Empty return here since how else do you want to check if the check needs to be enabled ¯\_(ツ)_/¯
        //   }
        // }
        if (matches[i].referee) {
          refereechannel.send(
            `🔔 <@${
              mapReferee(matches[i].referee).discordid
            }> You have a match to ref in ${matchInMinutes} minutes! Please prepare yourself. | \`Match #${
              matches[i].id
            }, ${matches[i].player1} vs ${matches[i].player2}\``
          );
          console.debug(
            `Refpinged ${mapReferee(matches[i].referee).name} in ${
              refereechannel.guild.name
            }#${refereechannel.name} for match #${matches[i].id}.`
          );
        } else {
          if (tournament.convars.refRolePingOnMissingRef) {
            refereechannel.send(
              `🔔 Dear <@&${tournament.refereeRoleId}>s, the match in ${matchInMinutes} minutes currently has no referee assigned! Please do so by signing up on the sheet. Thank you! | \`Match #${matches[i].id}, ${matches[i].player1} vs ${matches[i].player2}\``
            );
          }
        }
        if (tournament.convars.streamerPings && matches[i].streamer) {
          streamerchannel.send(
            `🔔 <@${
              mapStreamer(matches[i].streamer).discordid
            }> You have a match to stream in ${matchInMinutes} minutes! Please prepare yourself. | \`Match #${
              matches[i].id
            }, ${matches[i].player1} vs ${matches[i].player2}\``
          );
          console.debug(
            `Streampinged ${mapStreamer(matches[i].streamer).name} in ${
              streamerchannel.guild.name
            }#${streamerchannel.name} for match #${matches[i].id}.`
          );
        }
        if (
          tournament.convars.commentatorPings &&
          (matches[i].commentator1 || matches[i].commentator2)
        ) {
          if (matches[i].commentator1 && !matches[i].commentator2) {
            commentatorchannel.send(
              `🔔 <@${
                mapCommentator(matches[i].commentator1).discordid
              }> You have a match to commentate in ${matchInMinutes} minutes! Please prepare yourself. | \`Match #${
                matches[i].id
              }, ${matches[i].player1} vs ${matches[i].player2}\``
            );
            console.debug(
              `Commpinged ${mapCommentator(matches[i].commentator1).name} in ${
                commentatorchannel.guild.name
              }#${commentatorchannel.name} for match #${matches[i].id}.`
            );
          } else if (!matches[i].commentator1 && matches[i].commentator2) {
            commentatorchannel.send(
              `🔔 <@${
                mapCommentator(matches[i].commentator2).discordid
              }> You have a match to commentate in ${matchInMinutes} minutes! Please prepare yourself. | \`Match #${
                matches[i].id
              }, ${matches[i].player1} vs ${matches[i].player2}\``
            );
            console.debug(
              `Commpinged ${mapCommentator(matches[i].commentator2).name} in ${
                commentatorchannel.guild.name
              }#${commentatorchannel.name} for match #${matches[i].id}.`
            );
          } else {
            commentatorchannel.send(
              `🔔 <@${mapCommentator(matches[i].commentator1).discordid}> <@${
                mapCommentator(matches[i].commentator2).discordid
              }> You have a match to commentate in ${matchInMinutes} minutes! Please prepare yourself. | \`Match #${
                matches[i].id
              }, ${matches[i].player1} vs ${matches[i].player2}\``
            );
            console.debug(
              `Commpinged ${mapCommentator(matches[i].commentator1).name} and ${
                mapCommentator(matches[i].commentator2).name
              } in ${commentatorchannel.guild.name}#${
                commentatorchannel.name
              } for match #${matches[i].id}.`
            );
          }
        }
      }
    }
  }
  console.log(`Reminder function for ${tournament.acronym} executed!`);

  /**
   * Create an OAuth2 client with the given credentials, and then execute the
   * given callback function.
   * @param {Object} credentials The authorization client credentials.
   * @param {function} callback The callback to call with the authorized client.
   */
  function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
      client_id,
      client_secret,
      redirect_uris[0]
    );

    // Check if we have previously stored a token.
    fs.readFile("token.json", (err, token) => {
      if (err) return getNewToken(oAuth2Client, callback);
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client);
    });
  }

  /**
   * Get and store new token after prompting for user authorization, and then
   * execute the given callback with the authorized OAuth2 client.
   * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
   * @param {callback} callback The callback for the authorized client.
   */
  function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: "offline",
      scope: SCOPES,
    });
    console.log("Authorize this app by visiting this url:", authUrl);
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question("Enter the code from that page here: ", (code) => {
      rl.close();
      oAuth2Client.getToken(code, (err, token) => {
        if (err)
          return console.error(
            "Error while trying to retrieve access token",
            err
          );
        oAuth2Client.setCredentials(token);
        // Store the token to disk for later program executions
        fs.writeFile("token.json", JSON.stringify(token), (err) => {
          if (err) return console.error(err);
          console.log("Token stored to token.json");
        });
        callback(oAuth2Client);
      });
    });
  }

  /**
   * Prints the names and majors of students in a sample spreadsheet:
   * @see https://docs.google.com/spreadsheets/d/1jCzhvz30XNBQavaTOhAnVsrPBQIqyRa_oQOi9EvTkvQ/edit
   * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
   */
  function readSchedules(auth) {
    const sheets = google.sheets({ version: "v4", auth });
    sheets.spreadsheets.values.get(
      {
        spreadsheetId: tournament.spreadsheetId,
        range: tournament.dataRange,
      },
      (err, res) => {
        if (err) {
          if (err.message === "invalid_grant" || err.name === "invalid_grant") {
            Client.users
              .fetch("392359476151451663")
              .then((sinsa) =>
                sinsa.send("My token expired, please refresh it!")
              );
            Client.user.setPresence({
              activity: {
                name: "for a fix. Not in Service, please wait a while...",
                type: "LISTENING",
              },
              status: "dnd",
            });
          }
          return console.error("The API returned an error: " + err);
        }
        let rows = res.data.values;
        if (rows.length) {
          let i = 0;
          // Filling up match objects from sheet
          rows.map((row) => {
            matches[i] = {
              id: row[tournament.columns.id],
              date: row[tournament.columns.date],
              time: row[tournament.columns.time],
              player1: row[tournament.columns.player1],
              player2: row[tournament.columns.player2],
              referee: row[tournament.columns.referee],
              streamer: row[tournament.columns.streamer],
              commentator1: row[tournament.columns.commentator1],
              commentator2: row[tournament.columns.commentator2],
              pointsplayer1: row[tournament.columns.pointsplayer1],
              pointsplayer2: row[tournament.columns.pointsplayer2],
            };
            i++;
          });
        } else {
          console.log("No data found.");
        }
      }
    );
  }

  /**
   * Maps a referee from the sheet to the object from staff.json
   * @param requestingRef the ref name you'd like to map
   * @returns Referee a referee object from staff.json if everything goes well, else nothing will return.
   */
  function mapReferee(requestingRef) {
    for (let i = 0; i < tournament.staff.referees.length; i++) {
      if (
        tournament.staff.referees[i].sheet.toLowerCase() ===
        requestingRef.toLowerCase()
      ) {
        return tournament.staff.referees[i];
      }
    }
  }

  /**
   * Maps a streamer from the sheet to the object from staff.json
   * @param requestingStreamer the ref name you'd like to map
   * @returns Streamer a streamer object from staff.json if everything goes well, else nothing will return.
   */
  function mapStreamer(requestingStreamer) {
    for (let i = 0; i < tournament.staff.streamers.length; i++) {
      if (
        tournament.staff.streamers[i].sheet.toLowerCase() ===
        requestingStreamer.toLowerCase()
      ) {
        return tournament.staff.streamers[i];
      }
    }
  }

  /**
   * Maps a commentator from the sheet to the object from staff.json
   * @param requestingCommentator the ref name you'd like to map
   * @returns Commentator a commentator object from staff.json if everything goes well, else nothing will return.
   */
  function mapCommentator(requestingCommentator) {
    for (let i = 0; i < tournament.staff.commentators.length; i++) {
      if (
        tournament.staff.commentators[i].sheet.toLowerCase() ===
        requestingCommentator.toLowerCase()
      ) {
        return tournament.staff.commentators[i];
      }
    }
  }

  /**
   * Reformats a date (in sheets format) and time to the ISO format
   * @param date the date in format dd/mm/yyyy
   * @param time the time in format hh:mm:ss
   * @returns string a ISO timestamp, formed out of the provided parameters.
   */
  function getDate(date, time) {
    if (time.match("^\\d{1}:\\d{2}$")) {
      time = "0" + time;
    }
    let parsedDate;
    if (date.includes("/")) {
      const internaldate = date.split("/");
      parsedDate = Date.parse(
        `${internaldate[2]}-${internaldate[1]}-${internaldate[0]}T${time}`
      );
    } else if (date.includes("-")) {
      parsedDate = Date.parse(`${date}T${time}`);
    } else {
      throw new DOMException(
        "The Date format supplied by the sheet is not supported.",
        "DateFormatNotSupportedException"
      );
    }
    if (typeof parsedDate !== "number") {
      console.error(
        `${
          tournament.acronym
        } \t DateParseError \t parsing timestring "${time}" returned ${parsedDate} (typeof ${typeof parsedDate})!`
      );
      return null;
    } else {
      return parsedDate;
    }
  }
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function printLoadedTournaments(tournaments) {
  console.log("The following tournaments were loaded:");
  tournaments.forEach((tournament) => {
    if (tournament.active) {
      console.log(`${tournament.acronym}\t${tournament.name}`);
    }
  });
  console.log("End of loaded tournaments list.");
}

Client.login(token);
